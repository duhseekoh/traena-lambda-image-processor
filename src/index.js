'use strict';
import AWS from 'aws-sdk';
import sharp from 'sharp';
import util from 'util';
import path from 'path';

const s3 = new AWS.S3();

// constants
const SUPPORTED_EXT = ['.jpg', '.jpeg', '.png'];
const EXT_TO_OUTPUT_EXT = {
  '.jpg': '.jpeg',
  '.jpeg': '.jpeg',
  '.png': '.png',
};
const EXT_TO_FORMAT = {
  '.jpg': 'jpeg',
  '.jpeg': 'jpeg',
  '.png': 'png',
};
const EXT_TO_MIME_TYPE = {
  '.jpg': 'image/jpeg',
  '.jpeg': 'image/jpeg',
  '.png': 'image/png',
};
const IMAGE_SIZES = {
  tiny: { name: 'tiny', width: 150 },
  small: { name: 'small', width: 375 },
  medium: { name: 'medium', width: 750 },
  large: { name: 'large', width: 1536 },
  original: { name: 'original', width: null },
};
const STORAGE_CLASS = 'REDUCED_REDUNDANCY';

/**
 * Process an image input by resizing it, then uploading it to s3 as a sibling
 * to the original file.
 * e.g. If the imageSize is sm and the outputBucket is the same as in the input bucket then
 *  /somebucket/images/my-cat.png would result in /somebucket/images/my-cat-small.png
 **/
async function processAndUploadImage({
  inputBuffer,    // input image
  parsedInputKey, // s3 path parsed with node
  outputBucket,
  outputDirectory,
  imageSize,      // from IMAGE_SIZES constant
}) {
  // Setup
  const inputExt = parsedInputKey.ext.toLowerCase();
  const outputExtension = EXT_TO_OUTPUT_EXT[inputExt];
  const outputFormat = EXT_TO_FORMAT[inputExt];
  const outputMimeType = EXT_TO_MIME_TYPE[inputExt];
  const outputFilename = imageSize.name; // does not contain extension

  // Convert the image
  console.time(`convert-${imageSize.name}-image`);
  const processedImage = await sharp(inputBuffer)
    .rotate() // rotates based on EXIF tag (e.g. removes rotation metadata and actually rotates it)
    .resize(imageSize.width)
    .withoutEnlargement()
    .toFormat(outputFormat)
    .toBuffer({ resolveWithObject: true });
  console.timeEnd(`convert-${imageSize.name}-image`);

  console.log(`${imageSize.name} image info`);
  console.log(processedImage.info);
  console.log(`${imageSize.name} image data`);
  console.log(processedImage.data);

  // Upload the image
  const outputKey = path.format({
    dir: outputDirectory,
    ext: outputExtension,
    name: outputFilename,
  });

  console.time(`upload-${imageSize.name}-image`);
  const imageUploadSmallResponse = await s3.putObject({
      Bucket: outputBucket,
      Key: outputKey,
      Body: processedImage.data,
      ContentType: outputMimeType,
      StorageClass: STORAGE_CLASS
  }).promise();
  console.timeEnd(`upload-${imageSize.name}-image`);

  return {
    format: processedImage.info.format,
    width: processedImage.info.width,
    height: processedImage.info.height,
    filename: `${outputFilename}${outputExtension}`,
  };
}

exports.handler = async (event, context, callback) => {
  console.log('Received event:', JSON.stringify(event, null, 2));
  // Choose between event style (from programmatic call or s3 trigger)
  // At the moment, no good way to take in s3 events and output to same bucket, so turning off ability for that
  const rawInputKey = event.key; //(event.key || event.Records[0].s3.object.key);
  const inputKey = decodeURIComponent(rawInputKey.replace(/\+/g, " "));
  const parsedInputKey = path.parse(inputKey);
  const bucket = event.bucket; // || event.Records[0].s3.bucket.name;
  const inputExt = parsedInputKey.ext.toLowerCase(); // e.g. .jpg | .png | .jpeg
  const inputFormat = EXT_TO_FORMAT[inputExt];
  const outputDirectory = `${parsedInputKey.dir}/${parsedInputKey.name}`;

  if(!SUPPORTED_EXT.includes(inputExt)) {
    callback(`Unrecognized file extension ${inputExt}`);
    return;
  }

  // --- Download the image from S3, transform, and upload to a different S3 bucket.
  let imageDownloadResponse;
  try {
    console.time('download-original');
    imageDownloadResponse = await s3.getObject({
      Bucket: bucket,
      Key: inputKey
    }).promise();
    console.timeEnd('download-original');
  } catch (err) {
    callback(err);
    return;
  }
  console.log('download-response', imageDownloadResponse);

  // --- Generate images and upload asyncronously
  const tinyPromise = processAndUploadImage({
    inputBuffer: imageDownloadResponse.Body,
    parsedInputKey,
    outputBucket: bucket,
    outputDirectory,
    imageSize: IMAGE_SIZES.tiny,
  });

  const smallPromise = processAndUploadImage({
    inputBuffer: imageDownloadResponse.Body,
    parsedInputKey,
    outputBucket: bucket,
    outputDirectory,
    imageSize: IMAGE_SIZES.small,
  });

  const mediumPromise = processAndUploadImage({
    inputBuffer: imageDownloadResponse.Body,
    parsedInputKey,
    outputBucket: bucket,
    outputDirectory,
    imageSize: IMAGE_SIZES.medium,
  });

  const largePromise = processAndUploadImage({
    inputBuffer: imageDownloadResponse.Body,
    parsedInputKey,
    outputBucket: bucket,
    outputDirectory,
    imageSize: IMAGE_SIZES.large,
  });

  const originalPromise = processAndUploadImage({
    inputBuffer: imageDownloadResponse.Body,
    parsedInputKey,
    outputBucket: bucket,
    outputDirectory,
    imageSize: IMAGE_SIZES.original,
  });

  // --- Wait till they are all done, delete the original, then callback the lambda
  try {
    const imageOutputs = await Promise.all([tinyPromise, smallPromise, mediumPromise, largePromise, originalPromise])
      .then(([tinyOutput, smallOutput, mediumOutput, largeOutput, originalOutput]) => {
        return {
          [IMAGE_SIZES.tiny.name]: tinyOutput,
          [IMAGE_SIZES.small.name]: smallOutput,
          [IMAGE_SIZES.medium.name]: mediumOutput,
          [IMAGE_SIZES.large.name]: largeOutput,
          [IMAGE_SIZES.original.name]: originalOutput,
        };
      });

    // Delete the original image
    console.time('delete-original');
    await s3.deleteObject({
      Bucket: bucket,
      Key: inputKey
    }).promise();
    console.timeEnd('delete-original');

    // Success
    callback(null, {
      bucket,
      baseKey: outputDirectory,
      variations: imageOutputs,
    });
    return;
  } catch (err) {
    console.log(`Could not process or upload ${inputKey}`, err);
    callback(err);
    return;
  }

};
