# Overview

This lambda is setup to accept an s3 path to an image file (png or jpeg), and output several resized versions of that png or jpeg.

## Invoking

```
aws lambda invoke output.txt --function-name traena-image-processor-sandbox --payload file:///Users/dom/code/traena-workspace/lambda-image-processor/events/sample-event.json --log-type Tail --invocation-type RequestResponse
```
View the results in `output.txt`.

## Output

#### s3
An input of `images/my-image.png` will result in creation of 5 sibling files.
```
images/my-image-tiny.png // 150px width
images/my-image-small.png // 365px width
images/my-image-medium.png // 750px width
images/my-image-large.png // 1536px width
images/my-image-original.png // not resized, but includes any other processing
```

#### lambda response
On a successful processing invocation, the lambda will respond with
```
{
  bucket: 'my-bucket',
  baseKey: 'some-dir/image-processing/',
  variations: {
    tiny: {format, width, height, bucket, key},
    small: {format, width, height, bucket, key},
    medium: {format, width, height, bucket, key},
    large: {format, width, height, bucket, key},
    original: {format, width, height, bucket, key}
  }
}
```

## Events

Sample events are stored in `/events`. To invoke the lambda using one of these sample events, use the aws-cli.

The event itself looks like the following.
```
{
  key: 'images/test-image.png',
  bucket: 'traena-sandbox'
}
```

## Build / Deploy process

### Build
The build process (`yarn run build`) executes against `src/` and outputs to `build/`. It performs the following actions:
* remove `build/` if it exists
* transpile code in the `src/` directory using `babel` into `build/`
* execute `tools/build.js` to copy the `package.json` into the build directory
* execute `yarn install --production` in the context of the `build` directory

### Deploy

#### Locally to Sandbox
To deploy locally customize and execute the following command:

```
AWS_DESCRIPTION="<Your lambda description>" \
AWS_MEMORY_SIZE=512 \
AWS_TIMEOUT=20 \
AWS_RUNTIME=nodejs6.10 \
AWS_REGION=us-west-2 \
AWS_HANDLER=index.handler \
AWS_PROFILE=<profile from ~/.aws/credentials> \
AWS_FUNCTION_NAME=traena-image-processor-sandbox \
AWS_ROLE_ARN="arn:aws:iam::047975831122:role/traena-sandbox" \
PREBUILT_DIRECTORY=build \
node-lambda deploy --configFile deploy-sandbox.env
```

#### Circle Ci
* Ensure AWS credentials are configured in the `circleci.com` settings for the branch

## Sharp Dependency

The image processing library being used, Sharp (http://sharp.dimens.io/), must be compiled in the same environment the lambda will be executing in. Thus, it's not as simple as just including Sharp as an npm dependency and packaging it up on circleci. We are using a precompiled version of Sharp which was created on an AMI that Lambdas use during runtime. The sharp tarball is included in the source of this project. The last step of the npm build is to extract that tarball into the built node_modules directory. This is why Sharp is included in package.json as a devDependency as opposed to a production dependency.

* Read more here: http://sharp.dimens.io/en/stable/install/#aws-lambda
* This is the serverless project where the tarball was taken from: https://github.com/adieuadieu/serverless-sharp-image/tree/master/lambda-sharp/tarballs
